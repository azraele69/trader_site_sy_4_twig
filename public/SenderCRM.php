<?php


/*
 * Описание передаваемых данных
 *

В запросе необходимо передать следующие параметры:
name - имя клиента, обязательное поле
surname - фамилия, необязательное поле
middlename - отчество, необязательное поле
phone - телефон клиента, обязательное поле
email - электронная почта клиента, необязательное поле
comments - комментарий клиента, необязательное поле
office_id - идентификатор офиса, куда попадет заявка, обязательное поле. Например office_id=576050 "Дистанционное обучение Украина"
type_id - тип заявки, обязательное поле. Одно из значений:
1 - Обратный звонок
2 - Хочу учиться в офисе
3 -  Хочу инвестировать (ПерсТрейдер)
4 - Хочу учиться дистанционно в Москве
5 - Хочу инвест. в Москве (ПерсТрейдер)
6 - Мастер-класс в Москве
7 - Хочу учиться дистанционно
8 - Сайт ПТ: Вопрос с сайта
9 - Сайт ПТ: Стать инвестором
10 - Сайт ПТ: Стать трейдером
11 - Сайт ПТ: Вопрос трейдеру
12 - Хочу демо счет
13 - Мастер-инвест
14 - Хочу работать
15 - Конкурсы
16 - Акции
17 - Хочу бонус
18 - 24% годовых
19 - Вопрос менеджеру
20 - Хочу зарабатывать
21 - Хочу открыть счет
29 - Заказать обучающие материалы
30 - Хочу торговые сигналы
31 - Хочу электронную книгу
32 - Хочу VIP счет
33 - Мастер-инвест: мастер
34 - Хочу учиться: новичкам
35 - Телемагазин
36 - Бизнес семинар
37 - Школа миллионера. Забрать бонус
38 - Школа миллионера. Перестал играть
39 - Сайт БТ: Вопрос с сайта
40 - Сайт БТ: Стать инвестором
41 - Сайт БТ: Стать трейдером
42 - Сайт БТ: Вопрос трейдеру
43 - Школа миллионера. Регистрация
44 - Хочу стать партнером
45 - Хочу в демо конкурс
46 - Школа трейдера. Получить бонус 1000
47 - Школа трейдера. Перестал играть
48 - Холодная рассылка: Мастер-класс
49 - Финансовая справочная
50 - Торговые сигналы
51 - Хочу торговать на ВФБ
52 - АБТ: Хочу учиться в офисе
53 - АБТ: Хочу учиться дистанционно
54 - АБТ: Регистрация на вебинар
55 - АБТ: Обратный звонок
56 - Недозаполненная заявка
57 - Школа трейдера. Активный игрок
58 - Школа миллионера. Активный игрок
59 - Би-Центр: обучающие курсы
60 - Скачать торговый терминал
61 - Сайт СТ: Вопрос трейдеру
62 - Сайт СТ: Стать инвестором
63 - Феникс: скачать книгу
64 - Новости компании
65 - Би-Центр: регистрация
66 - Call-center: заявка из ТТГ
67 - Call-center: заявка из Академии Трейдинга
68 - Феникс online: скачать книгу
url_form - урл, с которого отправлена заявка, обязательное поле
url_local - урл первого захода на сайт, необязательное поле. Помогает в отслеживании рекламных компаний
url_referer - реферер для первого захода на сайт, необязательное поле. Помогает в отслеживании рекламных компаний
google_clientid - идентификатор устройста (браузера) при наличии на странице счетчика Google Analitycs
ip - ip-адрес клиента, необязательное поле. В crm-системк действует ограничение на прием не более 10 заявок с одного ip-адреса в час. Если параметр отсутствует, то используется ip адрес соединения, откуда пришла заявка.
contact_type_id - идентификатор социальной сети или меседжера. Может принимать одно из значений:
1 - "Facebook"
2 - "Вконтакте"
3 - "Одноклассники"
4 - "Skype"
5 - "ICQ"
6 - "Jabber"
7 - "Google+"
8 - "GoogleTalk"
9 - "LiveJournal"
90303 - "QQ"
contact_login - ссылка на страницу профиля или имя контакта в меседжере
timezone - часовой пояс клиента

Любые другие параметры могут передаваться, но они будут проигнорированы.


Длина любого аргумента не должна превышать 1000 символов.
Это правило устарело. Любые не ASCII-символы, т.е. символы с кодом больше 128 должны быть закодированы в виде "\XXXX", где XXXX-шестнадцатиричное представление символа в кодировке UCS-2. Пример, слово "Привет" должно быть передано как "\041f\0440\0438\0432\0435\0442". Любой одиночный "\" должен заменяться на двойной "\\".


Возвращаемое значение

Ответ в формате jsonp. В случае ошибки: код ошибки и текст ошибки. Пример:
formCallback(
 [
  {
   error: -20201,
   error_text: "Отсутствует параметр."
  }
 ]
)

В случае успешной отправки заявки: идентификатор сохраненной заявки в CRM-системе. Пример:
formCallback(
 [
  {
   id: 284639
  }
 ]
)


Пример заявки:

https://my.teletrade-dj.com/webform/crm/web_request_form_add?name=test&phone=7557575&email=name@test.com&type_id=2&office_id=576050&url_form=http://somesite.ru/

 */

class SenderCRM {

  public static function send($data=array(), $logger='') {

    $info = static::get_default_crm_info();
    $info = array_merge($info, $data);
    $info = static::clear_empty($info);
    $error = static::valid_crm_info($info);

    if(empty($error)) {
      $result = urldecode(http_build_query($info)); // urldecode()
      $id = -1;
      $id = static::getSslPage('https://my.teletrade-dj.com/webform/crm/web_request_form_add', $result);
      if($logger) {
        static::save_log($result, $id, $logger);
//        print "ID: $id --- $result" . PHP_EOL;
      }
      return true;
    }
    else {
      static::save_log($error . '\n'. serialize($info) . '\n\n\n', -2, 'error_log');
      return false;
    }
  }

  private static function clear_empty($info){
    $info = array_filter($info, function($value) { return !empty($value);});
    return $info;
  }

  public static function checkEmail($email) {
   // return preg_match("~([a-zA-Z0-9!#$%&amp;'*+-/=?^_`{|}~])@([a-zA-Z0-9-]).([a-zA-Z0-9]{2,4})~",$email);
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  public static function checkPhone($phone) {
    $phone = preg_replace('/\D/', '', $phone);
    return strlen($phone) > 9;
  }

  private static function valid_crm_info($info) {
    $error = '';

    $requests = ['name', 'email', 'office_id', 'type_id', 'url_form'];
    foreach($requests as $option){
      if(!isset($info[$option]) || empty($info[$option])) {
        $error .= "Not set '$option'; ";
      }
    }

    if(!static::checkEmail($info['email'])){
      $error .= 'Invalid email; ';
    }

    if($info['phone'] && !static::checkPhone($info['phone'])){
      $error .= 'Invalid Phone; ';
    }

    return $error;
  }

  private static function get_default_crm_info() {

    // Get Google Tag
    $google_clientid = '';
    if(isset($_COOKIE['_ga'])){
      $google_tag = $_COOKIE['_ga'];
      $pattern = '/GA\d+\.\d+\.(.+)$/i';
      $replacement = '${1}';
      $google_clientid = preg_replace($pattern, $replacement, $google_tag);
    }

    // Get user IP
    $ip = $_SERVER['REMOTE_ADDR'];
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    $url_referer = $_SERVER["HTTP_REFERER"];
    $url_form = static::url_origin( $_SERVER, false) . $_SERVER['REQUEST_URI'];
    $url_local = '';

    if($url_referer === '') $url_referer = $url_form;
    if($url_local === '') $url_local = $url_referer;
    if($url_local === '') $url_local = $url_form;


    $url_referer = rawurlencode($url_referer);
    $url_form = rawurlencode($url_form);
    $url_local = rawurlencode($url_local);

//    $address = $url_form;

    $info = [
      'name' => $_POST['name'] ?: '',
      'surname' => $_POST['surname'] ?: '',
      'middlename' => $_POST['middlename'] ?: '',
      'phone' => $_POST['phone'] ?: '',
      'email' => $_POST['email'] ?: '',
      'comments' => $_POST['comments'] ?: '',
      'office_id' => $_POST['office_id'] ?: '',
      'type_id' => $_POST['type_id'] ?: '',
      'url_form' => $url_form,
      'url_local' => $url_local,
      'url_referer' => $url_referer,
      'google_clientid' => $google_clientid,
      'ip' => $ip,
    ];

    return $info;
  }

  private static function url_origin( $s, $use_forwarded_host = false )
  {
    $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
    $sp       = strtolower( $s['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
    $port     = $s['SERVER_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
    $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host;
  }

  private static function getSslPage($url, $fields) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST,true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  private static function save_log($result, $id, $file){
    $content = "ID: $id --- $result" . PHP_EOL;
    $file_name= __DIR__ . '/'. $file . "___a01sjeJejsf.txt";
    if(!file_exists($file_name)){
      $fp = fopen($file_name, 'w');
      fclose($fp);
      chmod($file_name, 0777);
    }
    file_put_contents($file_name, PHP_EOL . $content, FILE_APPEND);
  }
  
}


