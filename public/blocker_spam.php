<?php

function checkMinInterval($data, $seconds) {
  $error = FALSE;
  $log_path = 'log_send_crm.txt';
  if (!file_exists($log_path)) {
    file_put_contents($log_path, '');
  }
  $logs_string = file_get_contents($log_path);
  $logs = json_decode($logs_string, TRUE);

  $now_date = date ('Y-m-d H:i:s');

  if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
  } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else {
    $ip = $_SERVER['REMOTE_ADDR'];
  }
  $ip = trim(explode(',', $ip)[0]);

  foreach (array_reverse($logs) as $log) {
    if ($log['ip'] === $ip) {
      $timeDifference = strtotime($now_date) - strtotime($log['datetime']);
      if ($timeDifference < $seconds) {
        $error = "Ваша заявка уже получена. С вами свяжутся наши менеджеры в самое ближайшее время.";
      }
      break;
    }
  }
  $data['datetime'] = $now_date;
  $data['ip'] = $ip;
  $data['spam'] = $error ? TRUE : FALSE;
  $logs[] = $data;
  file_put_contents($log_path, json_encode($logs, JSON_PRETTY_PRINT));
  return $error;
}
