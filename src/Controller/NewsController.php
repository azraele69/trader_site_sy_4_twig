<?php

namespace App\Controller;

use App\Entity\News;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller
{
  public function list(Request $request)
  {
    return $this->render('pages/news_list.html.twig', [
      'title' => 'Новости',
      'items' => $this->newsList($request),
    ]);
  }

  public function show($id)
  {
    return $this->render('pages/news_show.html.twig', [
      'title' => 'Новости',
      'item' => $this->news_show($id),
    ]);
  }

  public function sideBarBlock()
  {
    return $this->render('blocks/side_bar_block.html.twig', [
      'title' => 'Новости',
      'items' => $this->getDoctrine()->getRepository(News::class)->dataForBlock(),
    ]);
  }

  private function newsList($request)
  {
    $entity = $this->getDoctrine()->getRepository(News::class)->getAll();
    $paginator  = $this->get('knp_paginator');
    $appointments = $paginator->paginate(
      $entity,
      $request->query->getInt('page', 1),
      // Items per page
      10
    );

    return $appointments;
  }

  public function news_show($id)
  {
    $news = $this->getDoctrine()
      ->getRepository(News::class)
      ->find($id);

    if (!$news) {
      return false;
    }

    return $news;
  }
}
