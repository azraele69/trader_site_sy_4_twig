<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DirectorController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/director.html.twig', [
      'title' => 'РУКОВОДИТЕЛЬ',
    ]);
  }
}
