<?php

namespace App\Controller;

use App\Entity\Checks;
use App\Entity\Courses;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ShopFormController extends AbstractController
{

  public function index(Request $request,$id)
  {
    $form = $this->createFormBuilder()
      ->add('Last_Name', TextType::class, array(
        'label' => 'Фамилия *',
      ))
      ->add('Name', TextType::class, array(
        'label' => 'Имя *',
      ))
      ->add('Middle_Name', TextType::class, array(
        'label' => 'Отчество *',
      ))
      ->add('Email', TextType::class, array(
        'label' => 'email *',
      ))
      ->add('Phone', TextType::class, array(
        'label' => 'Телефон *',
      ))
      ->add('Oferta', CheckboxType::class, array(
        'label' => 'Согласен с договором оферты',
      ))
      ->add('Personal_Data', CheckboxType::class, array(
        'label' => 'Согласен на обработку персональных данных',
      ))
      ->add('save', SubmitType::class, ['label' => 'Купить'])
      ->getForm()
    ;

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      // $form->getData() holds the submitted values
      $form_data = $form->getData();
      $course_data = $this->extractCourse($id);
      $orderId = $this->createCheckNumber($course_data->getCheckCode());

      $this->AddCheck(
        $orderId,
        $course_data->getCoursesName(),
        $course_data->getCost(),
        $form_data['Last_Name'],
        $form_data['Name'],
        $form_data['Middle_Name'],
        $form_data['Phone'],
        $form_data['Email'],
        $request->getClientIp()
      );

      $html = $this->fenix_shop_get_form(
        $orderId,
        $course_data->getCost(),
        $form_data['Phone'],
        $form_data['Name'],
        $form_data['Email'],
        $course_data->getCoursesName()
      );

      $response = new Response();
      $response->setContent($html);
      return $response;
    }

    return $this->render('forms/shop.html.twig', [
      'form' => $form->createView(),
    ]);
  }

  public function fenix_shop_get_form($orderId, $amount, $phone, $name, $email, $serviceName)
  {
    $payment_parameters = http_build_query(array(
      "clientid" => $name . ' ' . $phone . ' ' . $email,
      "orderid" => $orderId . ' ' . $serviceName,
      "sum" => $amount,
      "client_email" => $email,
      "phone" => $phone,
      "service_name" => $serviceName,
      "cart" => json_encode([
        [
          'name' => $serviceName,
          'price' => $amount,
          'quantity' => 1,
          'sum' => $amount,
          'tax' => 'none',
          'tax_sum' => 0,
          'item_type' => 'service',
          'payment_type' => 'prepay',
        ],
      ]),
    ));

    $options = array("http" => array(
      "method" => "POST",
      "header" => "Content-type: application/x-www-form-urlencoded",
      "content" => $payment_parameters
    ));

    $context = stream_context_create($options);
    $response = file_get_contents('https://fenixtradingclub.server.paykeeper.ru/order/inline/', FALSE, $context);

    return $response;
  }

  private function extractCourse($id)
  {
    $course = $this->getDoctrine()
      ->getRepository(Courses::class)
      ->find($id);

    if (!$course) {
      throw $this->createNotFoundException(
        'No product found for id '.$id
      );
    }

    return $course;
  }

  private function AddCheck($check_number, $lesson_name, $cost, $user_seconds_name, $user_name, $patronymic, $phone, $email, $user_ip)
  {
    $em = $this->getDoctrine()->getManager();

    $check = new Checks();
    $check->setCheckNumber($check_number);
    $check->setLessonName($lesson_name);
    $check->setCost($cost);
    $check->setUserSecondName($user_seconds_name);
    $check->setUserName($user_name);
    $check->setPatronymic($patronymic);
    $check->setPhone($phone);
    $check->setEmail($email);
    $check->setUserIp($user_ip);

    $em->persist($check);
    $em->flush();
  }
  private function createCheckNumber($check_code)
  {
    $course = $this->getDoctrine()->getRepository(Checks::class)->ChecksCount();
    return $check_code.' '.$this->ZeroFill($course);
  }

  private function ZeroFill($check_number)
  {
    $check_number ++;
    $zeros = '00000000';
    $result = $zeros . $check_number;
    return substr($result, -8);
  }
}
