<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class StartUpController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/start_up.html.twig', [
      'title' => 'START UP: тренинг для начинающих',
    ]);
  }
}


