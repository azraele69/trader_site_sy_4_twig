<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MasterGroupController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/master_group.html.twig', [
      'title' => 'MASTER GROUP',
    ]);
  }
}
