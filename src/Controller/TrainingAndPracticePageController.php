<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TrainingAndPracticePageController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/training-practice.html.twig', [
      'title' => 'Обучение и  практика торговли: как заработать на разнице валютных курсов',
    ]);
  }
}
