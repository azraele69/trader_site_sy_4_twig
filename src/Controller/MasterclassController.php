<?php

namespace App\Controller;

use App\Entity\MassterClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MasterclassController extends AbstractController
{
  public function list()
  {
    return $this->render('pages/masterclass_list.html.twig', [
      'title' => 'МАСТЕР-КЛАССЫ ПО ТРЕЙДИНГУ',
      'items' => $this->masterclassList(),
    ]);
  }

  public function show($id)
  {
    return $this->render('pages/masterclass_show.html.twig', [
      'title' => 'МАСТЕР-КЛАССЫ ПО ТРЕЙДИНГУ',
      'item' => $this->masterclassShow($id),
    ]);
  }

  private function masterclassList()
  {
    return $this->getDoctrine()->getRepository(MassterClass::class)->findAll();
  }

  private function masterclassShow($id)
  {
    $news = $this->getDoctrine()
      ->getRepository(MassterClass::class)
      ->find($id);
    if (!$news) {
      return false;
    }
    return $news;
  }

}
