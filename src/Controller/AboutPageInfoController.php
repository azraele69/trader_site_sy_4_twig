<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AboutPageInfoController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/about_page_info.html.twig', [
      'title' => 'Об институте',
    ]);
  }
}
