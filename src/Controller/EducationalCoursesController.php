<?php

namespace App\Controller;

use App\Entity\EducationalCourses;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EducationalCoursesController extends AbstractController
{

  public function minimalVersionBlock()
  {
    return $this->render('blocks/static_block/educational_courses_minimal_block.html.twig', [
      'items' => $this->blockData(),
    ]);
  }

  public function fullVersionBlock()
  {
    return $this->render('blocks/static_block/educational_courses_full_block.html.twig', [
      'items' => $this->blockData(),
    ]);
  }

  private function blockData()
  {
    $data = $this->getDoctrine()
      ->getRepository(EducationalCourses::class)
      ->findAll();

    if (!$data) {
      return false;
    }

    return $data;
  }
}
