<?php

namespace App\Controller;

use App\Entity\EventAuthor;
use App\Entity\EventProgram;
use App\Entity\FenixCalendarEvent;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class TimeTablePageController extends AbstractController
{
  private $item_for_page = 7;
  private $item_for_block = 4;

  public function index()
  {
    return $this->render('pages/time_table.html.twig', [
      'title' => 'Расписание занятий',
      'data' => $this->calendarBuild($this->item_for_page),
      'authors' => $this->AuthorList(),
      'programs' => $this->ProgramList(),
    ]);
  }

  public function block()
  {
    return $this->render('blocks/calendar_block.html.twig', [
      'data' => $this->calendarBuild($this->item_for_block),
    ]);
  }

  private function AuthorList()
  {
    $data = $this->getDoctrine()
      ->getRepository(EventAuthor::class)
      ->findAll();

    if (!$data) {
      throw $this->createNotFoundException(
        'No data missing'
      );
    }

    return $data;
  }

  private function ProgramList()
  {
    $data = $this->getDoctrine()
      ->getRepository(EventProgram::class)
      ->findAll();

    if (!$data) {
      throw $this->createNotFoundException(
        'No data missing'
      );
    }

    return $data;
  }

  private function calendarBuild($day)
  {
    $build = array();
    foreach ($this->dateCreator($day) as $date) {
      $data = array(
        'date' => $date,
        'items' => $this->dailySchedule($date),
      );
      array_push($build, $data);
    }
    return $build;
  }

  private function dailySchedule($date)
  {
    $data = $this->getDoctrine()->getRepository(FenixCalendarEvent::class)->EjectLessonByDate($date);
    if (!$data) {
      return false;
    }
    return $data;
  }

  private function dateCreator($day)
  {
    $data_array = array();
    for ($x = 0; $x < $day; $x++) {
      array_push($data_array, date('Y-m-d', strtotime("+" . $x . " day")));
    }
    return $data_array;
  }
}
