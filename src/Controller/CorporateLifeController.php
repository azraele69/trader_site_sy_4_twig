<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CorporateLifeController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/corporate_life.html.twig', [
      'title' => 'Корпоративная жизнь',
    ]);
  }
}
