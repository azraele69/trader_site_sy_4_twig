<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MediaAboutInstitutePageController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/media_about_institute.html.twig', [
      'title' => 'СМИ о нашем институте',
    ]);
  }
}
