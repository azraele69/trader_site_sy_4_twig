<?php

namespace App\Controller;

use App\Entity\Teacher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class TeachersController extends AbstractController
{
  public function list()
  {
    return $this->render('pages/teacher_list.html.twig', [
      'title' => 'Преподаватели',
      'items' => $this->teacherList(),
    ]);
  }

  public function show($id)
  {
    return $this->render('pages/teacher_show.html.twig', [
      'title' => 'Преподаватели',
      'items' => $this->teacher_show($id),
    ]);
  }

  private function teacherList()
  {
    $data = $this->getDoctrine()
      ->getRepository(Teacher::class)
      ->findAll();

    if (!$data) {
      throw $this->createNotFoundException(
        'No data missing'
      );
    }

    return $data;
  }

  public function teacher_show($id)
  {
    $teacher = $this->getDoctrine()
      ->getRepository(Teacher::class)
      ->find($id);

    if (!$teacher) {
      throw $this->createNotFoundException(
        'No product found for id '.$id
      );
    }

    return $teacher;
  }
}
