<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class IntensiveController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/intensive.html.twig', [
      'title' => 'INTENSIVE: от теории к практике торговли',
    ]);
  }
}
