<?php

namespace App\Controller;

use App\Entity\OurAdvantages;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OurAdvantagesController extends AbstractController
{

  public function index()
  {
    return $this->render('blocks/static_block/advantages_block.html.twig', [
      'items' => $this->blockData(),
    ]);
  }

  private function blockData()
  {
    $data = $this->getDoctrine()
      ->getRepository(OurAdvantages::class)
      ->findAll();

    if (!$data) {
      throw $this->createNotFoundException(
        'No data missing'
      );
    }

    return $data;
  }
}
