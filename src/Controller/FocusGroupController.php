<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FocusGroupController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/focus_group.html.twig', [
      'title' => 'FOCUS GROUP - Коуч с трейдером',
    ]);
  }
}
