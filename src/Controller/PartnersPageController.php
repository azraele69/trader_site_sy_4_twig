<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PartnersPageController extends AbstractController
{
    public function index()
    {
        return $this->render('pages/partners.html.twig', [
          'title' => 'Партнеры',
        ]);
    }
}
