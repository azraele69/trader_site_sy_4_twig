<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TargetsAndGoalsController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/targets_and_goals.html.twig', [
      'title' => 'Цели и задачи',
    ]);
  }
}
