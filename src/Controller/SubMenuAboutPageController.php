<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SubMenuAboutPageController extends Controller
{
  private $menuLevel = 2;

  public function index()
  {
    $menu_manager = $this->container->get('prodigious_sonata_menu.manager');

    $menu = $menu_manager->loadByAlias('sub_menu_about_page');
    $menuItems = $menu_manager->getMenuItems($menu, true);

    return $this->render('menu/sub_menu_about_page.html.twig', [
      'menu_title' => 'Об институте',
      'procreator' => '/about-page-info',
      'items' => $menuItems,
      'request' => $this->routPars(),
    ]);
  }

  private function routPars ()
  {
    $activeUrl = $this->get('request_stack')->getMasterRequest()->getRequestUri();
    $request = explode("/", $activeUrl);
    if (!isset($request[$this->menuLevel])){
      return ' ';
    }
    return '/'.$request[$this->menuLevel];
  }
}
