<?php

namespace App\Controller;

use App\Entity\Analytics;
use App\Entity\AnalyticsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AnalyticsPageController extends Controller
{
  public function index()
  {
    return $this->render('pages/analytics.html.twig', [
      'title' => 'Аналитика',
      'data' => $this->indexBuild(),
    ]);
  }

  public function indexType($type,Request $request)
  {
    return $this->render('pages/analytics_type_list.html.twig', [
      'title' => 'Аналитика',
      'type' => $type,
      'type_name' => $this->typeName($type),
      'items' => $this->indexTypeBuild($type,$request),
    ]);
  }

  public function indexShow($id)
  {
    return $this->render('pages/analytics_show.html.twig', [
      'title' => 'Аналитика',
      'item' => $this->indexShowBuild($id),
    ]);
  }

  private function typeName ($alias)
  {
    return $this->getDoctrine()->getRepository(AnalyticsType::class)->findOneBy(array('type_alias' => $alias));
  }

  public function blockLastAnalytics()
  {
    return $this->render('blocks/analytics_block.html.twig', [
      'items' => $this->getDoctrine()->getRepository(Analytics::class)->getDataBlock(),
    ]);
  }

  private function indexBuild()
  {
    $build = array();
    $types = $this->getTypes();
    foreach ($types as $type) {
      $data = array(
        'type' => $type->getType(),
        'link' => $type->getTypeAlias(),
        'items' => $this->getDoctrine()->getRepository(Analytics::class)->getAnalyticByType($type),
      );
      array_push($build, $data);
    }
    return $build;
  }


  private function indexTypeBuild($type, Request $request)
  {
    $entity = $this->getDoctrine()->getRepository(Analytics::class)->getAnalyticByAlias($type);
    $paginator  = $this->get('knp_paginator');
    $appointments = $paginator->paginate(
      $entity,
      $request->query->getInt('page', 1),
      // Items per page
      10
    );

    return $appointments;
  }

  private function indexShowBuild($id)
  {
    $news = $this->getDoctrine()
      ->getRepository(Analytics::class)
      ->find($id);
    if (!$news) {
      return false;
    }

    return $news;
  }

  private function getTypes()
  {
    $data = $this->getDoctrine()
      ->getRepository(AnalyticsType::class)
      ->findAll();
    if (!$data) {
      return false;
    }
    return $data;
  }


}
