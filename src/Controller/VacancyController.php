<?php

namespace App\Controller;

use App\Entity\Vacancy;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class VacancyController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/vacancy.html.twig', [
      'title' => 'Вакансии',
      'items' => $this->vacancyList(),
    ]);
  }

  private function vacancyList()
  {
    $data = $this->getDoctrine()
      ->getRepository(Vacancy::class)
      ->findAll();

    if (!$data) {
      throw $this->createNotFoundException(
        'No data missing'
      );
    }

    return $data;
  }
}
