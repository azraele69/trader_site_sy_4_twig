<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FooterMenuController extends Controller
{

  public function index()
  {
    $menu_manager = $this->container->get('prodigious_sonata_menu.manager');

    $menu = $menu_manager->loadByAlias('footer_menu');
    $menuItems = $menu_manager->getMenuItems($menu, true);

    return $this->render('menu/footer_menu.html.twig', [
      'menu' => $menuItems,
    ]);
  }
}
