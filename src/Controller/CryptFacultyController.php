<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CryptFacultyController extends AbstractController
{
    public function index()
    {
        return $this->render('pages/crypt_faculty.html.twig', [
          'title' => 'КРИПТОВАЛЮТНЫЙ ФАКУЛЬТЕТ',
        ]);
    }
}
