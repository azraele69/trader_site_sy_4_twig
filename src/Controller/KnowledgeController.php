<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class KnowledgeController extends AbstractController
{
  public function index()
  {
    return $this->render('pages/knowledge.html.twig', [
      'title' => 'Научная Работа',
    ]);
  }
}
