<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HeaderMenuController extends Controller
{
  private $menuLevel = 1;

  public function index()
  {
    $menu_manager = $this->container->get('prodigious_sonata_menu.manager');

    $menu_desktop = $menu_manager->loadByAlias('header_desktop');
    $desktop_menuItems = $menu_manager->getMenuItems($menu_desktop, true);

    $menu_mobile = $menu_manager->loadByAlias('header_mobile');
    $mobile_menuItems = $menu_manager->getMenuItems($menu_mobile, true);

    return $this->render('menu/header_menu.html.twig', [
      'Desktop' => $desktop_menuItems,
      'Mobile' => $mobile_menuItems,
      'request' => $this->routPars(),
    ]);
  }

  private function routPars (){
    $activeUrl = $this->get('request_stack')->getMasterRequest()->getRequestUri();
    $request = explode("/", $activeUrl);
    return '/'.$request[$this->menuLevel];
  }

}
