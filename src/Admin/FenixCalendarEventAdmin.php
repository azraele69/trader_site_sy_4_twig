<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\EventAuthor;
use App\Entity\EventLessonType;
use App\Entity\EventProgram;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class FenixCalendarEventAdmin extends AbstractAdmin
{

  protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
  {
    $datagridMapper
      ->add('id')
      ->add('title')
      ->add('event_date')
      ->add('event_time');
  }

  protected function configureListFields(ListMapper $listMapper): void
  {
    $listMapper
      ->add('id')
      ->add('title')
      ->add('event_date')
      ->add('event_time')
//      ->add('eventAuthor.name')
      ->add('_action', null, [
        'actions' => [
          'show' => [],
          'edit' => [],
          'delete' => [],
        ],
      ]);
  }

  protected function configureFormFields(FormMapper $formMapper): void
  {
    $formMapper
      ->add('title')
      ->add('event_date')
      ->add('event_time')
      ->add('eventAuthor', EntityType::class, [
        'class' => EventAuthor::class,
        'choice_label' => 'name',
      ])
      ->add('eventProgram', EntityType::class, [
        'class' => EventProgram::class,
        'choice_label' => 'name',
      ])
      ->add('eventLessonType', EntityType::class, [
        'class' => EventLessonType::class,
        'choice_label' => 'name',
      ])
    ;
  }

  protected function configureShowFields(ShowMapper $showMapper): void
  {
    $showMapper
      ->add('id')
      ->add('title')
      ->add('event_date')
      ->add('event_time');
  }
}
