<?php

declare(strict_types=1);

namespace App\Admin;

use App\Entity\Courses;
use App\Entity\Teacher;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

final class MassterClassAdmin extends AbstractAdmin
{

  protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
  {
    $datagridMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('duration');
  }

  protected function configureListFields(ListMapper $listMapper): void
  {
    $listMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('teacher.name')
      ->add('course.courses_name')
      ->add('duration')
      ->add('_action', null, [
        'actions' => [
          'show' => [],
          'edit' => [],
          'delete' => [],
        ],
      ]);
  }

  protected function configureFormFields(FormMapper $formMapper): void
  {
    $formMapper
      ->add('title')
      ->add('body')
      ->add('teacher', EntityType::class, [
        'class' => Teacher::class,
        'choice_label' => 'name',
      ])
      ->add('course', EntityType::class, [
        'class' => Courses::class,
        'choice_label' => 'courses_name',
      ])
      ->add('duration');
  }

  protected function configureShowFields(ShowMapper $showMapper): void
  {
    $showMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('duration');
  }
}
