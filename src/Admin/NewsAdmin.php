<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Vich\UploaderBundle\Form\Type\VichImageType;

final class NewsAdmin extends AbstractAdmin
{

  protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
  {
    $datagridMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('date')
      ->add('counter')
      ->add('imageName')
      ->add('updatedAt');
  }

  protected function configureListFields(ListMapper $listMapper): void
  {
    $listMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('date')
      ->add('counter')
      ->add('imageName')
      ->add('updatedAt')
      ->add('_action', null, [
        'actions' => [
          'show' => [],
          'edit' => [],
          'delete' => [],
        ],
      ])
    ;
  }

  protected function configureFormFields(FormMapper $formMapper): void
  {
    $formMapper
      ->add('title')
      ->add('body')
      ->add('date')
//      ->add('counter')
      ->add('imageFile', VichImageType::class)
    ;
  }

  protected function configureShowFields(ShowMapper $showMapper): void
  {
    $showMapper
      ->add('id')
      ->add('title')
      ->add('body')
      ->add('date')
      ->add('counter')
      ->add('imageName')
      ->add('updatedAt')
    ;
  }
}
