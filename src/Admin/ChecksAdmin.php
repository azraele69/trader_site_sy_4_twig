<?php

declare(strict_types=1);

namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

final class ChecksAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $datagridMapper): void
    {
        $datagridMapper
			->add('id')
			->add('check_number')
			->add('lesson_name')
			->add('cost')
			->add('date')
			->add('user_second_name')
			->add('user_name')
			->add('patronymic')
			->add('phone')
			->add('email')
			->add('user_ip')
			;
    }

    protected function configureListFields(ListMapper $listMapper): void
    {
        $listMapper
			->add('check_number')
			->add('lesson_name')
			->add('cost')
			->add('date')
			->add('user_second_name')
			->add('user_name')
			->add('patronymic')
			->add('phone')
			->add('email')
			->add('user_ip')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper): void
    {
        $formMapper
			->add('check_number')
			->add('lesson_name')
			->add('cost')
			->add('date')
			->add('user_second_name')
			->add('user_name')
			->add('patronymic')
			->add('phone')
			->add('email')
			->add('user_ip')
			;
    }

    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
			->add('id')
			->add('check_number')
			->add('lesson_name')
			->add('cost')
			->add('date')
			->add('user_second_name')
			->add('user_name')
			->add('patronymic')
			->add('phone')
			->add('email')
			->add('user_ip')
			;
    }
}
