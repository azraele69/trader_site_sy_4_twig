<?php

namespace App\Repository;

use App\Entity\OurAdvantages;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method OurAdvantages|null find($id, $lockMode = null, $lockVersion = null)
 * @method OurAdvantages|null findOneBy(array $criteria, array $orderBy = null)
 * @method OurAdvantages[]    findAll()
 * @method OurAdvantages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OurAdvantagesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, OurAdvantages::class);
    }

    // /**
    //  * @return OurAdvantages[] Returns an array of OurAdvantages objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OurAdvantages
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
