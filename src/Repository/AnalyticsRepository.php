<?php

namespace App\Repository;

use App\Entity\Analytics;
use App\Entity\AnalyticsType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Analytics|null find($id, $lockMode = null, $lockVersion = null)
 * @method Analytics|null findOneBy(array $criteria, array $orderBy = null)
 * @method Analytics[]    findAll()
 * @method Analytics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnalyticsRepository extends ServiceEntityRepository
{
  public function __construct(RegistryInterface $registry)
  {
    parent::__construct($registry, Analytics::class);
  }

  public function getAnalyticByType($type)
  {
    $qb = $this->createQueryBuilder('a')
      ->andWhere('a.analytics_type = :type')
      ->setParameter('type', $type)
      ->orderBy('a.updatedAt', 'DESC')
      ->setMaxResults(3)
      ->getQuery();

    return $qb->execute();
  }

  public function getAnalyticByAlias($alias)
  {
    $qb = $this->createQueryBuilder('p')
      ->innerJoin('p.analytics_type', 'c')
      ->addSelect('c')
      ->andWhere('c.type_alias = :alias')
      ->setParameter('alias', $alias)
      ->orderBy('p.updatedAt', 'DESC')
      ->getQuery()
      ->getResult();

    return $qb;
  }

  public function getDataBlock ()
  {
    return $this->createQueryBuilder('a')
      ->orderBy('a.updatedAt', 'DESC')
      ->setMaxResults(4)
      ->getQuery()
      ->getResult()
      ;
  }

  // /**
  //  * @return Analytics[] Returns an array of Analytics objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('a')
          ->andWhere('a.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('a.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */

  /*
  public function findOneBySomeField($value): ?Analytics
  {
      return $this->createQueryBuilder('a')
          ->andWhere('a.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
}
