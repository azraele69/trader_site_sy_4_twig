<?php

namespace App\Repository;

use App\Entity\FenixCalendarEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FenixCalendarEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method FenixCalendarEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method FenixCalendarEvent[]    findAll()
 * @method FenixCalendarEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FenixCalendarEventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FenixCalendarEvent::class);
    }

    public function EjectLessonByDate ($date)
    {
      $qb = $this->createQueryBuilder('p')
        ->andWhere('p.event_date = :date')
        ->setParameter('date', $date)
        ->orderBy('p.event_time', 'ASC')
        ->getQuery();

      return $qb->execute();
    }

    // /**
    //  * @return FenixCalendarEvent[] Returns an array of FenixCalendarEvent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FenixCalendarEvent
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
