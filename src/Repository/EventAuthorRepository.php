<?php

namespace App\Repository;

use App\Entity\EventAuthor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventAuthor|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventAuthor|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventAuthor[]    findAll()
 * @method EventAuthor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventAuthorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventAuthor::class);
    }

    // /**
    //  * @return EventAuthor[] Returns an array of EventAuthor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventAuthor
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
