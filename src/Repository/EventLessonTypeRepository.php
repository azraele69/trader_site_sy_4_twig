<?php

namespace App\Repository;

use App\Entity\EventLessonType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventLessonType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventLessonType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventLessonType[]    findAll()
 * @method EventLessonType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventLessonTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventLessonType::class);
    }

    // /**
    //  * @return EventLessonType[] Returns an array of EventLessonType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EventLessonType
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
