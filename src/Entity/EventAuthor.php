<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventAuthorRepository")
 */
class EventAuthor
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FenixCalendarEvent", mappedBy="event_author")
     */
    private $event_author;

    public function __construct()
    {
        $this->event_author = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FenixCalendarEvent[]
     */
    public function getEventAuthor(): Collection
    {
        return $this->event_author;
    }

    public function addEventAuthor(FenixCalendarEvent $eventAuthor): self
    {
        if (!$this->event_author->contains($eventAuthor)) {
            $this->event_author[] = $eventAuthor;
            $eventAuthor->setEventAuthor($this);
        }

        return $this;
    }

    public function removeEventAuthor(FenixCalendarEvent $eventAuthor): self
    {
        if ($this->event_author->contains($eventAuthor)) {
            $this->event_author->removeElement($eventAuthor);
            // set the owning side to null (unless already changed)
            if ($eventAuthor->getEventAuthor() === $this) {
                $eventAuthor->setEventAuthor(null);
            }
        }

        return $this;
    }
}
