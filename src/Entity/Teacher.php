<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TeacherRepository")
 * @Vich\Uploadable
 */
class Teacher
{
  public function __construct()
  {
    $this->updatedAt = new \DateTime();
    $this->master_class = new ArrayCollection();
  }

  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $name;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $position;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $education;

  /**
   * @ORM\Column(type="text")
   */
  private $about_me;

  /**
   * NOTE: This is not a mapped field of entity metadata, just a simple property.
   *
   * @Vich\UploadableField(mapping="teachers", fileNameProperty="imageName")
   *
   * @var File
   */
  private $imageFile;

  /**
   * @ORM\Column(type="string", length=255)
   *
   * @var string
   */
  private $imageName;

  /**
   * @ORM\Column(type="datetime")
   *
   * @var \DateTime
   */
  private $updatedAt;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $experience;

  /**
   * @ORM\OneToMany(targetEntity="App\Entity\MassterClass", mappedBy="teacher")
   */
  private $master_class;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getName(): ?string
  {
    return $this->name;
  }

  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  public function getPosition(): ?string
  {
    return $this->position;
  }

  public function setPosition(string $position): self
  {
    $this->position = $position;

    return $this;
  }

  public function getEducation(): ?string
  {
    return $this->education;
  }

  public function setEducation(string $education): self
  {
    $this->education = $education;

    return $this;
  }

  public function getAboutMe(): ?string
  {
    return $this->about_me;
  }

  public function setAboutMe(string $about_me): self
  {
    $this->about_me = $about_me;

    return $this;
  }

  public function setImageFile(?File $imageFile = null): void
  {
    $this->imageFile = $imageFile;

    if (null !== $imageFile) {
      // It is required that at least one field changes if you are using doctrine
      // otherwise the event listeners won't be called and the file is lost
      $this->updatedAt = new \DateTimeImmutable();
    }
  }

  public function getImageFile(): ?File
  {
    return $this->imageFile;
  }

  public function setImageName(?string $imageName): void
  {
    $this->imageName = $imageName;
  }

  public function getImageName(): ?string
  {
    return $this->imageName;
  }

  /**
   * @return \DateTime
   */
  public function getUpdatedAt(): \DateTime
  {
    return $this->updatedAt;
  }

  /**
   * @param \DateTime $updatedAt
   */
  public function setUpdatedAt(\DateTime $updatedAt)
  {
    $this->updatedAt = $updatedAt;
  }

  public function getExperience(): ?string
  {
      return $this->experience;
  }

  public function setExperience(string $experience): self
  {
      $this->experience = $experience;

      return $this;
  }

  /**
   * @return Collection|MassterClass[]
   */
  public function getMasterClass(): Collection
  {
      return $this->master_class;
  }

  public function addMasterClass(MassterClass $masterClass): self
  {
      if (!$this->master_class->contains($masterClass)) {
          $this->master_class[] = $masterClass;
          $masterClass->setTeacher($this);
      }

      return $this;
  }

  public function removeMasterClass(MassterClass $masterClass): self
  {
      if ($this->master_class->contains($masterClass)) {
          $this->master_class->removeElement($masterClass);
          // set the owning side to null (unless already changed)
          if ($masterClass->getTeacher() === $this) {
              $masterClass->setTeacher(null);
          }
      }

      return $this;
  }

}
