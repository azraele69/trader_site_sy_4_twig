<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChecksRepository")
 */
class Checks
{
  public function __construct()
  {
    $this->date = new \DateTime();
  }

  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=100)
   */
  private $check_number;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $lesson_name;

  /**
   * @ORM\Column(type="integer")
   */
  private $cost;

  /**
   * @ORM\Column(type="datetime")
   */
  private $date;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $user_second_name;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $user_name;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $patronymic;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $phone;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $email;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $user_ip;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getCheckNumber(): ?string
  {
    return $this->check_number;
  }

  public function setCheckNumber(string $check_number): self
  {
    $this->check_number = $check_number;

    return $this;
  }

  public function getLessonName(): ?string
  {
    return $this->lesson_name;
  }

  public function setLessonName(string $lesson_name): self
  {
    $this->lesson_name = $lesson_name;

    return $this;
  }

  public function getCost(): ?int
  {
    return $this->cost;
  }

  public function setCost(int $cost): self
  {
    $this->cost = $cost;

    return $this;
  }
//: \DateTime
  public function getDate(): ?\DateTimeInterface
  {
    return $this->date;
  }

  public function setDate(\DateTimeInterface $date): self
  {
    $this->date = $date;

    return $this;
  }

  public function getUserSecondName(): ?string
  {
    return $this->user_second_name;
  }

  public function setUserSecondName(string $user_second_name): self
  {
    $this->user_second_name = $user_second_name;

    return $this;
  }

  public function getUserName(): ?string
  {
    return $this->user_name;
  }

  public function setUserName(string $user_name): self
  {
    $this->user_name = $user_name;

    return $this;
  }

  public function getPatronymic(): ?string
  {
    return $this->patronymic;
  }

  public function setPatronymic(string $patronymic): self
  {
    $this->patronymic = $patronymic;

    return $this;
  }

  public function getPhone(): ?string
  {
    return $this->phone;
  }

  public function setPhone(string $phone): self
  {
    $this->phone = $phone;

    return $this;
  }

  public function getEmail(): ?string
  {
    return $this->email;
  }

  public function setEmail(string $email): self
  {
    $this->email = $email;

    return $this;
  }

  public function getUserIp(): ?string
  {
    return $this->user_ip;
  }

  public function setUserIp(string $user_ip): self
  {
    $this->user_ip = $user_ip;

    return $this;
  }
}
