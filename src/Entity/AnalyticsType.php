<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnalyticsTypeRepository")
 */
class AnalyticsType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $type_alias;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Analytics", mappedBy="analytics_type")
     */
    private $analytics_type;

    public function __construct()
    {
        $this->analytics_type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Analytics[]
     */
    public function getAnalyticsType(): Collection
    {
        return $this->analytics_type;
    }

    public function addAnalyticsType(Analytics $analyticsType): self
    {
        if (!$this->analytics_type->contains($analyticsType)) {
            $this->analytics_type[] = $analyticsType;
            $analyticsType->setAnalyticsType($this);
        }

        return $this;
    }

    public function removeAnalyticsType(Analytics $analyticsType): self
    {
        if ($this->analytics_type->contains($analyticsType)) {
            $this->analytics_type->removeElement($analyticsType);
            // set the owning side to null (unless already changed)
            if ($analyticsType->getAnalyticsType() === $this) {
                $analyticsType->setAnalyticsType(null);
            }
        }

        return $this;
    }

  /**
   * @return mixed
   */
  public function getTypeAlias()
  {
    return $this->type_alias;
  }

  /**
   * @param mixed $type_alias
   */
  public function setTypeAlias($type_alias)
  {
    $this->type_alias = $type_alias;
  }
}
