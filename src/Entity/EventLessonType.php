<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventLessonTypeRepository")
 */
class EventLessonType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FenixCalendarEvent", mappedBy="event_lesson_type")
     */
    private $event_lesson_type;

    public function __construct()
    {
        $this->event_lesson_type = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FenixCalendarEvent[]
     */
    public function getEventLessonType(): Collection
    {
        return $this->event_lesson_type;
    }

    public function addEventLessonType(FenixCalendarEvent $eventLessonType): self
    {
        if (!$this->event_lesson_type->contains($eventLessonType)) {
            $this->event_lesson_type[] = $eventLessonType;
            $eventLessonType->setEventLessonType($this);
        }

        return $this;
    }

    public function removeEventLessonType(FenixCalendarEvent $eventLessonType): self
    {
        if ($this->event_lesson_type->contains($eventLessonType)) {
            $this->event_lesson_type->removeElement($eventLessonType);
            // set the owning side to null (unless already changed)
            if ($eventLessonType->getEventLessonType() === $this) {
                $eventLessonType->setEventLessonType(null);
            }
        }

        return $this;
    }
}
