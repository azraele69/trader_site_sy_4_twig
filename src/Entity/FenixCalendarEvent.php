<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FenixCalendarEventRepository")
 */
class FenixCalendarEvent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="date")
     */
    private $event_date;

    /**
     * @ORM\Column(type="time")
     */
    private $event_time;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventAuthor", inversedBy="event_author")
     */
    private $event_author;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventProgram", inversedBy="event_program")
     */
    private $event_program;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventLessonType", inversedBy="event_lesson_type")
     */
    private $event_lesson_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getEventDate(): ?\DateTimeInterface
    {
        return $this->event_date;
    }

    public function setEventDate(\DateTimeInterface $event_date): self
    {
        $this->event_date = $event_date;

        return $this;
    }

    public function getEventTime(): ?\DateTimeInterface
    {
        return $this->event_time;
    }

    public function setEventTime(\DateTimeInterface $event_time): self
    {
        $this->event_time = $event_time;

        return $this;
    }

    public function getEventAuthor(): ?EventAuthor
    {
        return $this->event_author;
    }

    public function setEventAuthor(?EventAuthor $event_author): self
    {
        $this->event_author = $event_author;

        return $this;
    }

    public function getEventProgram(): ?EventProgram
    {
        return $this->event_program;
    }

    public function setEventProgram(?EventProgram $event_program): self
    {
        $this->event_program = $event_program;

        return $this;
    }

    public function getEventLessonType(): ?EventLessonType
    {
        return $this->event_lesson_type;
    }

    public function setEventLessonType(?EventLessonType $event_lesson_type): self
    {
        $this->event_lesson_type = $event_lesson_type;

        return $this;
    }
}
