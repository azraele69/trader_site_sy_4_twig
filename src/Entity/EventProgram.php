<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventProgramRepository")
 */
class EventProgram
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FenixCalendarEvent", mappedBy="event_program")
     */
    private $event_program;

    public function __construct()
    {
        $this->event_program = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FenixCalendarEvent[]
     */
    public function getEventProgram(): Collection
    {
        return $this->event_program;
    }

    public function addEventProgram(FenixCalendarEvent $eventProgram): self
    {
        if (!$this->event_program->contains($eventProgram)) {
            $this->event_program[] = $eventProgram;
            $eventProgram->setEventProgram($this);
        }

        return $this;
    }

    public function removeEventProgram(FenixCalendarEvent $eventProgram): self
    {
        if ($this->event_program->contains($eventProgram)) {
            $this->event_program->removeElement($eventProgram);
            // set the owning side to null (unless already changed)
            if ($eventProgram->getEventProgram() === $this) {
                $eventProgram->setEventProgram(null);
            }
        }

        return $this;
    }
}
