<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursesRepository")
 */
class Courses
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $courses_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $cost;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $check_code;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MassterClass", mappedBy="course")
     */
    private $master_class;

    public function __construct()
    {
        $this->master_class = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoursesName(): ?string
    {
        return $this->courses_name;
    }

    public function setCoursesName(string $courses_name): self
    {
        $this->courses_name = $courses_name;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getCheckCode(): ?int
    {
        return $this->check_code;
    }

    public function setCheckCode(?int $check_code): self
    {
        $this->check_code = $check_code;

        return $this;
    }

    /**
     * @return Collection|MassterClass[]
     */
    public function getMasterClass()
    {
        return $this->master_class;
    }

    public function addMasterClass(MassterClass $masterClass): self
    {
        if (!$this->master_class->contains($masterClass)) {
            $this->master_class[] = $masterClass;
            $masterClass->setCourse($this);
        }

        return $this;
    }

    public function removeMasterClass(MassterClass $masterClass): self
    {
        if ($this->master_class->contains($masterClass)) {
            $this->master_class->removeElement($masterClass);
            // set the owning side to null (unless already changed)
            if ($masterClass->getCourse() === $this) {
                $masterClass->setCourse(null);
            }
        }

        return $this;
    }

  /**
   * @return ArrayCollection
   */
//  public function __toString() {
//    return $this->master_class;
//  }

}
