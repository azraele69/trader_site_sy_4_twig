<?php

namespace App\Entity;
//  Сущность блока Образовательные курсы
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EducationalCoursesRepository")
 * @Vich\Uploadable
 */
class EducationalCourses
{
  public function __construct()
  {
    $this->updatedAt = new \DateTime();
  }
  /**
   * @ORM\Id()
   * @ORM\GeneratedValue()
   * @ORM\Column(type="integer")
   */
  private $id;

  /**
   * @ORM\Column(type="string", length=255)
   */
  private $title;

  /**
   * @ORM\Column(type="text")
   */
  private $body;

  /**
   * @ORM\Column(type="string", length=255, nullable=true)
   */
  private $full_page;

  /**
   * @ORM\Column(type="integer", nullable=true)
   */
  private $number_of_lessons;

  /**
   * NOTE: This is not a mapped field of entity metadata, just a simple property.
   *
   * @Vich\UploadableField(mapping="icons", fileNameProperty="imageName")
   *
   * @var File
   */
  private $imageFile;

  /**
   * @ORM\Column(type="string", length=255)
   *
   * @var string
   */
  private $imageName;

  /**
   * @ORM\Column(type="datetime", nullable=true)
   *
   * @var \DateTime
   */
  private $updatedAt;

  public function getId(): ?int
  {
    return $this->id;
  }

  public function getTitle(): ?string
  {
    return $this->title;
  }

  public function setTitle(string $title): self
  {
    $this->title = $title;

    return $this;
  }

  public function getBody(): ?string
  {
    return $this->body;
  }

  public function setBody(string $body): self
  {
    $this->body = $body;

    return $this;
  }

  public function getFullPage(): ?string
  {
    return $this->full_page;
  }

  public function setFullPage(string $full_page): self
  {
    $this->full_page = $full_page;

    return $this;
  }

  public function getNumberOfLessons(): ?int
  {
    return $this->number_of_lessons;
  }

  public function setNumberOfLessons(int $number_of_lessons): self
  {
    $this->number_of_lessons = $number_of_lessons;

    return $this;
  }

  public function setImageFile(?File $imageFile = null): void
  {
    $this->imageFile = $imageFile;

    if (null !== $imageFile) {
      // It is required that at least one field changes if you are using doctrine
      // otherwise the event listeners won't be called and the file is lost
      $this->updatedAt = new \DateTimeImmutable();
    }
  }

  public function getImageFile(): ?File
  {
    return $this->imageFile;
  }

  public function setImageName(?string $imageName): void
  {
    $this->imageName = $imageName;
  }

  public function getImageName(): ?string
  {
    return $this->imageName;
  }

  /**
   * @return \DateTime
   */
  public function getUpdatedAt(): \DateTime
  {
    return $this->updatedAt;
  }

  /**
   * @param \DateTime $updatedAt
   */
  public function setUpdatedAt(\DateTime $updatedAt)
  {
    $this->updatedAt = $updatedAt;
  }
}
